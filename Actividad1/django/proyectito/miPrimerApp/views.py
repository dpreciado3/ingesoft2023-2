from django.shortcuts import render, HttpResponse
from .models import *

# Create your views here.
def index(request):

    grupo1 = Estudiante.objects.filter(grupo = 1)
    grupo4 = Estudiante.objects.filter(grupo = 4)
    mismo_apellido = Estudiante.objects.raw('SELECT DISTINCT t1.* FROM miPrimerApp_estudiante t1 INNER JOIN miPrimerApp_estudiante t2 ON t1.apellidos = t2.apellidos AND t1.numCta != t2.numCta ORDER BY t1.apellidos')
    misma_edad = Estudiante.objects.raw('SELECT DISTINCT t1.* FROM miPrimerApp_estudiante t1 INNER JOIN miPrimerApp_estudiante t2 ON t1.edad = t2.edad AND t1.numCta != t2.numCta ORDER BY t1.edad')
    misma_edad_grupo3 = Estudiante.objects.raw('SELECT DISTINCT t1.* FROM (SELECT * FROM miPrimerApp_estudiante WHERE grupo_id = 3) t1 INNER JOIN (SELECT * FROM miPrimerApp_estudiante WHERE grupo_id = 3) t2 ON t1.edad = t2.edad AND t1.numCta != t2.numCta ORDER BY t1.edad')
    todos = Estudiante.objects.all()
    
    return render(request, "index.html", {'grupo1':grupo1, 'grupo4':grupo4, 'mismo_apellido':mismo_apellido, 'misma_edad':misma_edad, 'misma_edad_grupo3':misma_edad_grupo3, 'todos':todos})